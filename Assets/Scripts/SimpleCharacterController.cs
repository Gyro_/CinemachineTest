using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCharacterController : MonoBehaviour
{
    Camera main;

    Vector3 moveDir;

    Vector3 movVector;

    CharacterController characterController;

    public float turnSmoothTime = 0.1f;

    float turnSmoothVelocity;

    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        main = Camera.main;
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        ReadInput();

        Movement();

        characterController.Move(movVector * speed * Time.deltaTime /* 0.02f*/);
    }

    void ReadInput()
    {
        if (Input.GetKey(KeyCode.A))
           moveDir.x = -1;     
        if (Input.GetKey(KeyCode.D))
            moveDir.x = 1;    
        if (Input.GetKey(KeyCode.W))
            moveDir.z = 1;  
        if (Input.GetKey(KeyCode.S))
            moveDir.z = -1; 

        if (Input.GetKeyUp(KeyCode.A))
           moveDir.x = 0;     
        if (Input.GetKeyUp(KeyCode.D))
            moveDir.x = 0;    
        if (Input.GetKeyUp(KeyCode.W))
            moveDir.z = 0;  
        if (Input.GetKeyUp(KeyCode.S))
            moveDir.z = 0;
            
             
    }

    void Movement()
    {
        Vector3 dir = main.transform.rotation * moveDir;

        movVector.x = dir.x;
        movVector.z = dir.z;

        //movVector.Normalize();
    }
}
